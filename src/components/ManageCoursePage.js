import React, { useState, useEffect } from "react";
import CourseForm from "./CourseForm";
// import * as courseApi from "../api/courseApi";
import * as courseActions from "../actions/courseActions";
import courseStore from "../stores/courseStore";
import { toast } from "react-toastify";

const ManageCoursePage = props => {
  const [errors, setErrors] = useState({});
  const [courses, setCourses] = useState(courseStore.getCourses());
  const [course, setCourse] = useState({
    id: null,
    slug: "",
    title: "",
    authorId: null,
    category: ""
  });

  useEffect(() => {
    courseStore.addChangeListener(onChange);
    const slug = props.match.params.slug;
    if (courses.length === 0) courseActions.loadCourses();
    else if (slug) setCourse(courseStore.getCourseBySlug(slug));
    // courseApi.getCourseBySlug(slug).then(_course => setCourse(_course));
    return () => courseStore.removeChangeListener(onChange);
  }, [courses.length, props.match.params.slug]);

  const onChange = () => {
    setCourses(courseStore.getCourses());
  }

  const handleChange = ({ target }) => {
    //updateCourse[e.target.name] = e.target.value;
    setCourse({ ...course, [target.name]: target.value });
  };

  const formIsValid = () => {
    const _errors = {};
    if (!course.title) _errors.title = "Title is required";
    if (!course.authorId) _errors.authorId = "Author ID is required";
    if (!course.category) _errors.category = "Category is required";
    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  };

  const handleSubmit = event => {
    event.preventDefault();
    if (!formIsValid()) return;
    
    courseActions.createCourse(course).then(() => {
      props.history.push("/courses");
      toast.success("Course save!!!");
    });
  };

  return (
    <>
      <h1>Manage Course</h1>
      <CourseForm
        errors={errors}
        course={course}
        onChange={handleChange}
        onSubmit={handleSubmit}
      />
      <p>{props.match.params.slug}</p>
    </>
  );
};

export default ManageCoursePage;
