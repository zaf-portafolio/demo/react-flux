import React, { useState, useEffect } from "react";
// import { getCourses } from "../api/courseApi";
import coursesStore from "../stores/courseStore";
import CoursesList from "./CoursesList";
import { Link } from "react-router-dom";
import { loadCourses, deleteCourses } from '../actions/courseActions';

function CoursesPage() {
  const [courses, setCourses] = useState(coursesStore.getCourses());

  useEffect(() => {
    // const fetchCourses = async () => {
    //   const _courses = await getCourses();
    //   setCourses(_courses);
    // };
    // fetchCourses();
    coursesStore.addChangeListener(onChange);
    if(coursesStore.getCourses().length === 0) loadCourses();

    return () => coursesStore.removeChangeListener(onChange); //cleanup on unmount
  }, []);

  const onChange = () => {
    setCourses(coursesStore.getCourses());
  };

  return (
    <>
      <h1>CoursesPage</h1>
      <Link to="/course" className="btn btn-primary">
        Add Course
      </Link>
      <CoursesList courses={courses} deleteCourse={deleteCourses}/>
    </>
  );
}

export default CoursesPage;
