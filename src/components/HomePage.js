import React from 'react';

function HomePage(){
    return <div className='jumbotron'>
        <h1>Administration</h1>
        <p>React, Flux, and React Router</p>
    </div>
}

export default HomePage;
